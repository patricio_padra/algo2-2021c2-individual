#include <iostream>
#include <list>
using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha {
  public:
    Fecha(uint mes, uint dia);
    uint mes();
    uint dia();
    void incrementar_dia();
    // Completar declaraciones funciones
    #if EJ >= 9 // Para ejercicio 9
    bool operator==(Fecha o);
    #endif

  private:
    int mes_;
    int  dia_;
    //Completar miembros internos
};

Fecha::Fecha(uint mes, uint dia) : mes_(mes) , dia_(dia) {}

uint Fecha::dia() {
    return dia_;
    }

    uint Fecha::mes()  {
    return mes_;
}

void Fecha::incrementar_dia() {
    if ( dia_ < dias_en_mes(mes_)) {
        dia_++;
    } else{(dia_=1) && (mes_++) ;}
}


ostream& operator<<(ostream& os, Fecha f) {
    os  << f.dia() << "/" << f.mes() ;
    return os;
}



#if EJ >= 9
bool Fecha::operator==(Fecha o) {
    bool igual_dia = this->dia_ == o.dia() and
            this->mes_ == o.mes();
    // Completar iguadad (ej 9)
   return igual_dia;
}


#endif

// Ejercicio 11, 12
class Horario {
public:
    Horario(uint hora, uint min);
    uint hora();
    uint min();
    bool operator==(Horario h);
    bool operator<(Horario h);
private:
int hora_;
int min_;
};

Horario::Horario(uint hora, uint min):hora_(hora), min_(min) {}

uint Horario::min() {
    return min_;
}

uint Horario::hora() {
    return hora_;
}
// Clase Horario
ostream& operator<<(ostream& os, Horario h) {
    os  << h.hora() << ":" << h.min() ;
    return os;
}
bool Horario::operator==(Horario h) {
    return hora_ == h.hora() and min_ == h.min();
    // Completar iguadad (ej 9)
}

bool Horario::operator<(Horario h) {
    return hora_ < h.hora() || hora_ == h.hora()  and min_ < h.min();
}
// Ejercicio 13

// Clase Recordatorio



class Horario;
class Fecha;
using MENSAJE = string;

class  Recordatorio {
public:
    Fecha f();
    Recordatorio(Fecha, Horario, MENSAJE);
   MENSAJE rec();
   Horario h();
   bool operator<(Recordatorio r);
;
private:
    MENSAJE rec_;
    Fecha f_;
    Horario h_;
};



MENSAJE Recordatorio::rec(){
 return this ->rec_;
}
Horario Recordatorio::h() {
    return h_;
}
Fecha Recordatorio::f()  {
    return f_;
}

Recordatorio::Recordatorio(Fecha f , Horario h, string REC): f_(f),h_(h), rec_(REC) {};

ostream& operator<<(ostream& os, Recordatorio r) {
    os  << r.rec() << " @ " << r.f()<< " " << r.h() ;
    return os;
}

bool Recordatorio::operator<(Recordatorio r) {
    return  h_ < r.h();
}

// Ejercicio 14

// Clase Agenda
class Agenda {
public:
    Agenda(Fecha fecha_inicial);
   void agregar_recordatorio(Recordatorio rec);
    void incrementar_dia();
   list<Recordatorio> recordatorios_de_hoy();
   Fecha hoy();


private:
    Fecha hoy_;
    list<Recordatorio> recordatorios_;
};

Agenda::Agenda(Fecha fecha_inicial): hoy_(fecha_inicial), recordatorios_() {}


Fecha Agenda::hoy()  {
    return hoy_;
}
void Agenda::incrementar_dia() {
    hoy_.incrementar_dia();
}

void Agenda::agregar_recordatorio(Recordatorio r) {
    recordatorios_.push_back(r);

}

list<Recordatorio> Agenda::recordatorios_de_hoy(){
    list<Recordatorio> ret;
    for (Recordatorio r: recordatorios_ ) {
        if (r.f() == hoy_){
            ret.push_back(r);
        }
    }
    ret.sort();
    return ret;
}


ostream& operator<<(ostream& os, Agenda a) {
    os  << a.hoy() <<endl;
    os << "=====" << endl;
    for (Recordatorio r : a.recordatorios_de_hoy()) {
        os << r << endl;
    };
    return os;
}